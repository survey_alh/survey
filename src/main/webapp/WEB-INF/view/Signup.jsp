<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規登録</title>
</head>
<body>
	<%@ include file="Header.jsp"%>

	<h2>新規登録</h2>
	<div class="main-contents">
	<%@ include file="ErrorMessages.jsp" %>

	<form:form modelAttribute="userForm">
		<div>
			<label>ログインID</label>
			<form:input path="loginId"/>
		</div>
		<div>
			<label>名前</label>
			<form:input path="name"/>
		</div>
		<div>
			<label>パスワード</label>
			<form:input  type="password" path="password"/>
		</div>
		<div>
			<label>パスワード（確認用）</label>
			<form:input type="password" path="passwordConfirmation"/>
		</div>

		<input type="submit" >
	</form:form>
	</div>

</body>
</html>