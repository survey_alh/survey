<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8>
<title>アンケート</title>
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<link href="<c:url value="/resources/css/survey.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/survey.js" />"></script>

</head>
<body>
	<%@ include file="Header.jsp"%>

	<div class="main-contents">
		<div id="survey">
		<h2><c:out value="${surveys[1].surveyTitle }" /></h2>
			<form:form method="POST" modelAttribute="apForm" id="answerquestion">
				<c:forEach items="${apForm.getAnswerForm() }" varStatus="status">
					<div class="questions" id="${status.index }">
						<div class="questionTitle" style="text-align: left;">
							問<c:out value="${surveys[status.index].questionNumber}" />
							<c:out value="${surveys[status.index].title}" />
						</div>
						<div class="choiceA">
							<div class="choice-box">
								<c:out value="${surveys[status.index].choiceA }" />
							</div>

							<form:radiobutton id="rbA${status.index }" label="◎" value="2"
								path="answerForm[${status.index}].driveNumber" />
							<form:radiobutton id="rbA${status.index }" label="○" value="1"
								path="answerForm[${status.index}].driveNumber" />
							<form:radiobutton id="rbA${status.index }" label="×" value="0"
								path="answerForm[${status.index}].driveNumber" checked="checked" />
						</div>
						<div class="choiceB">
							<div class="choice-box">
							<c:out value="${surveys[status.index].choiceB }" />
							</div>
							<form:radiobutton id="rbB${status.index }" label="◎" value="2"
								path="answerForm[${status.index}].analyzeNumber" />
							<form:radiobutton id="rbB${status.index }" label="○" value="1"
								path="answerForm[${status.index}].analyzeNumber" />
							<form:radiobutton id="rbB${status.index }" label="×" value="0"
								path="answerForm[${status.index}].analyzeNumber"
								checked="checked" />
						</div>
						<div class="choiceC">
							<div class="choice-box">
							<c:out value="${surveys[status.index].choiceC }" />
							</div>
							<form:radiobutton id="rbC${status.index }" label="◎" value="2"
								path="answerForm[${status.index}].createNumber" />
							<form:radiobutton id="rbC${status.index }" label="○" value="1"
								path="answerForm[${status.index}].createNumber" />
							<form:radiobutton id="rbC${status.index }" label="×" value="0"
								path="answerForm[${status.index}].createNumber"
								checked="checked" />
						</div>
						<div class="choiceD">
							<div class="choice-box">
							<c:out value="${surveys[status.index].choiceD }" />
							</div>
							<form:radiobutton id="rbD${status.index }" label="◎" value="2"
								path="answerForm[${status.index}].volunteerNumber" />
							<form:radiobutton id="rbD${status.index }" label="○" value="1"
								path="answerForm[${status.index}].volunteerNumber" />
							<form:radiobutton id="rbD${status.index }" label="×" value="0"
								path="answerForm[${status.index}].volunteerNumber"
								checked="checked" />
						</div>
						<form:hidden path="answerForm[${status.index}].userId"
							value="${loginUser.id }" />
						<form:hidden path="answerForm[${status.index}].questionId"
							value="${surveys[status.index].questionId}" />
						<form:hidden path="answerForm[${status.index}].surveyId"
							value="${surveys[status.index].surveyId}" />
						<c:if test="${status.index == 9 }">
							<input class="select_survey_btn" type="submit" id="submit" value="アンケートに回答" />
						</c:if>
					</div>

				</c:forEach>
			</form:form>
			<div id="next" class="select_survey_btn">next</div>
			<div id="back" >back</div>
		</div>
	</div>

</body>
</html>