<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
</head>
<body>
	<%@ include file="Header.jsp"%>
	<div class="main-contents">
		<h2>ログイン</h2>

		<%@ include file="ErrorMessages.jsp" %>

		<form:form modelAttribute="loginForm">
			<div>
				<label>ログインID</label>
				<form:input path="loginId" />
			</div>
			<div>
				<label>パスワード</label>
				<form:input type="password" path="password" />
			</div>

			<input type="submit">
		</form:form>
	</div>
</body>
</html>