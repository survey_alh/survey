<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<header class="site-header clearfix">
		<h1 class="site-logo">
			<a href="./" id="logo-decoration">Survey</a>
		</h1>
		<nav class="gnav">
			<ul class="gnav__menu">
				<c:if test="${empty loginUser}">
					<li class="gnav__menu__item"><a href="./login">ログイン</a></li>
					<li class="gnav__menu__item"><a href="./signup">サインアップ</a></li>
				</c:if>
				<c:if test="${ not empty loginUser }">
					<li class="gnav__menu__item"><a href="./history">履歴</a></li>
					<li class="gnav__menu__item"><a href="./users">ユーザー一覧</a></li>
					<li class="gnav__menu__item"><a href="./logout">ログアウト</a></li>
				</c:if>
			</ul>
		</nav>
</header>