<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>履歴</title>
</head>
<body>
	<%@ include file="Header.jsp"%>
	<div class="main-contents">
		<h2>履歴</h2>
		<div class="chart-history">
			<canvas id="myChart"></canvas>
		</div>

		<table class="history-table">
			<tr>
				<th>回答日時</th>
				<th>性格</th>
				<th>思考</th>
			</tr>

			<c:forEach items="${results}" var="result">
				<tr>
					<td><fmt:formatDate value="${result.createdDate}"
							pattern="yyyy/MM/dd" /></td>
					<td><c:out value="${result.personalityType}" /></td>
					<td><c:out value="${result.thinkingType}" /></td>
				</tr>
			</c:forEach>
		</table>

		<c:forEach items="${results}" var="result">
			<div id="createdDate" hidden>
				<fmt:formatDate value="${result.createdDate}"
							pattern="yyyy/MM/dd" />
			</div>
			<div id="driveNumber" hidden>
				<c:out value="${result.driveNumber}" />
			</div>
			<div id="volunteerNumber" hidden>
				<c:out value="${result.volunteerNumber}" />
			</div>
			<div id="analyzeNumber" hidden>
				<c:out value="${result.analyzeNumber}" />
			</div>
			<div id="createNumber" hidden>
				<c:out value="${result.createNumber}" />
			</div>
		</c:forEach>
	</div>
</body>
<script>
	var ctx = document.getElementById("myChart");
	var created_date = document.getElementById("createdDate").textContent;
	var drive_number = document.getElementById("driveNumber").textContent;
	var volunteer_number = document.getElementById("volunteerNumber").textContent;
	var analyze_number = document.getElementById("analyzeNumber").textContent;
	var create_number = document.getElementById("createNumber").textContent;

	var myChart = new Chart(ctx, {
		type : 'radar',
		data : {
			labels : [ "ドライブ", "クリエイト", "ボランティア", "アナライズ" ],
			datasets : [ {
				label : created_date,
				backgroundColor : "rgba(153,255,51,0.4)",
				borderColor : "rgba(153,255,51,1)",
				data : [ drive_number, create_number, volunteer_number,
						analyze_number ]
			} ]
		},
		options : {
			scale : {
				ticks : {
					max : 20,
					min : 0
				}
			}
		}
	});
</script>
</html>