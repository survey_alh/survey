<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>マイページ</title>
</head>
<body>

	<%@ include file="Header.jsp"%>

	<div class="main-contents">
		<div class="survey-form">
			<form action="survey">
				<select name="id">
					<c:forEach items="${surveys }" var="survey">
						<option value="<c:out value="${survey.id }"/>"><c:out
								value="${survey.surveyTitle}" /></option>
					</c:forEach>
				</select> <input type="submit" class="square_btn select_survey_btn submit" value="このアンケートへ" />
			</form>
		</div>

		<!-- 最新の診断結果をチャートで表示 -->
		<div class="mypage-chart">
			<h3>最新の診断結果</h3>
			<!-- 診断内容 -->
			<div class="mypage-table">
				<table>
					<thead>
						<tr>
							<th>性格</th>
							<th>思考</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><c:out value="${newResult.personalityType }" /></td>
							<td><c:out value="${newResult.thinkingType }" /></td>
						</tr>
					</tbody>
				</table>
			</div>
			<canvas id="myChart"></canvas>
		</div>
		<!-- チャートの値用 -->
		<div id="driveNumber" hidden>
			<c:out value="${newResult.driveNumber}" />
		</div>
		<div id="volunteerNumber" hidden>
			<c:out value="${newResult.volunteerNumber}" />
		</div>
		<div id="analyzeNumber" hidden>
			<c:out value="${newResult.analyzeNumber}" />
		</div>
		<div id="createNumber" hidden>
			<c:out value="${newResult.createNumber}" />
		</div>
	</div>
</body>
<script>
	var ctx = document.getElementById("myChart");
	var drive_number = document.getElementById("driveNumber").textContent;
	var volunteer_number = document.getElementById("volunteerNumber").textContent;
	var analyze_number = document.getElementById("analyzeNumber").textContent;
	var create_number = document.getElementById("createNumber").textContent;
	var myChart = new Chart(ctx, {
		type : 'radar',
		data : {
			labels : [ "ドライブ", "クリエイト", "ボランティア", "アナライズ" ],
			datasets : [ {
				label : 'apples',
				backgroundColor : "rgba(153,255,51,0.4)",
				borderColor : "rgba(153,255,51,1)",
				data : [ drive_number, create_number, volunteer_number,
						analyze_number ]
			} ]
		},
		options : {
			scale : {
				ticks : {
					max : 20,
					min : 0
				}
			}
		}
	});
</script>
</html>