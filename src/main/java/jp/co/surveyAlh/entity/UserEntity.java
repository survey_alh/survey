package jp.co.surveyAlh.entity;

import java.sql.Date;

public class UserEntity {
	private int id;
	private String loginId;
	private String name;
	private String password;
	private Date createdDate;
	private Date updatedDate;

	// 診断結果の値
	private int driveNumber;
	private int volunteerNumber;
	private int analyzeNumber;
	private int createNumber;
	public int getDriveNumber() {
		return driveNumber;
	}
	public void setDriveNumber(int driveNumber) {
		this.driveNumber = driveNumber;
	}
	public int getVolunteerNumber() {
		return volunteerNumber;
	}
	public void setVolunteerNumber(int volunteerNumber) {
		this.volunteerNumber = volunteerNumber;
	}
	public int getAnalyzeNumber() {
		return analyzeNumber;
	}
	public void setAnalyzeNumber(int analyzeNumber) {
		this.analyzeNumber = analyzeNumber;
	}
	public int getCreateNumber() {
		return createNumber;
	}
	public void setCreateNumber(int createNumber) {
		this.createNumber = createNumber;
	}
	private int resultX;

	public int getResultX() {
		return resultX;
	}
	public void setResultX(int resultX) {
		this.resultX = resultX;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
