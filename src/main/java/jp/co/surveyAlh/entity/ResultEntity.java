package jp.co.surveyAlh.entity;

import java.util.Date;

public class ResultEntity {
	private int id;
	private int userId;
	private int surveyId;
	private int driveNumber;
	private int volunteerNumber;
	private int analyzeNumber;
	private int createNumber;
	private Date createdDate;
	private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}
	public int getDriveNumber() {
		return driveNumber;
	}
	public void setDriveNumber(int driveNumber) {
		this.driveNumber = driveNumber;
	}
	public int getVolunteerNumber() {
		return volunteerNumber;
	}
	public void setVolunteerNumber(int volunteerNumber) {
		this.volunteerNumber = volunteerNumber;
	}
	public int getAnalyzeNumber() {
		return analyzeNumber;
	}
	public void setAnalyzeNumber(int analyzeNumber) {
		this.analyzeNumber = analyzeNumber;
	}
	public int getCreateNumber() {
		return createNumber;
	}
	public void setCreateNumber(int createNumber) {
		this.createNumber = createNumber;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
