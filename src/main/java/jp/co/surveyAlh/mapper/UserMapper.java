package jp.co.surveyAlh.mapper;

import java.util.List;

import jp.co.surveyAlh.entity.UserEntity;

public interface UserMapper {

	int insert(UserEntity user);
	UserEntity getUser(String loginId, String password);
	List<UserEntity> getUsers();
	UserEntity getUserByLoginId(String loginId);

}
