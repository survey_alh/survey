package jp.co.surveyAlh.mapper;

import org.springframework.stereotype.Repository;

import jp.co.surveyAlh.entity.AnswerEntity;

@Repository
public interface AnswerMapper {
	void setAnswer(AnswerEntity entity);
}
