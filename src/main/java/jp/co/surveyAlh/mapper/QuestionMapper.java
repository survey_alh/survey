package jp.co.surveyAlh.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import jp.co.surveyAlh.entity.QuestionEntity;

@Repository
public interface QuestionMapper {

	List<QuestionEntity> getQuestions(int id);
}
