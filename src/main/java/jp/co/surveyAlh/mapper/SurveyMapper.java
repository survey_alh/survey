package jp.co.surveyAlh.mapper;

import java.util.List;

import jp.co.surveyAlh.entity.SurveyEntity;

public interface SurveyMapper {
	SurveyEntity getSurvey(int id);
	List<SurveyEntity> getSurveys();
}
