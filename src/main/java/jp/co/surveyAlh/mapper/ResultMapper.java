package jp.co.surveyAlh.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import jp.co.surveyAlh.entity.ResultEntity;

@Repository
public interface ResultMapper {
	List<ResultEntity> getResults(int userId);
	void setResult(ResultEntity entity);
	ResultEntity getMaxId();
	ResultEntity getNewResult(int userId);
	List<ResultEntity>  getNewResults();
}
