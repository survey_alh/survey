package jp.co.surveyAlh.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.surveyAlh.dto.SurveyDto;
import jp.co.surveyAlh.entity.AnswerEntity;
import jp.co.surveyAlh.form.AnswerForm;
import jp.co.surveyAlh.mapper.AnswerMapper;

@Service
public class AnswerService {

	@Autowired
	private ResultService resultService;

	@Autowired
	private AnswerMapper answerMapper;

	public List<AnswerForm> getAnswerForm(List<SurveyDto> surveys) {
		List<AnswerForm> forms = new ArrayList<AnswerForm>();
		for(SurveyDto survey: surveys){
			AnswerForm form = new AnswerForm();
			forms.add(form);
		}

		return forms;
	}

	public void setAnswer(List<AnswerForm> aForms){
		int resultId = resultService.setResult(aForms);
		List<AnswerEntity> entitys = convertToEntity(aForms, resultId);
		for(AnswerEntity entity:entitys){
			answerMapper.setAnswer(entity);
		}
	}

	private List<AnswerEntity> convertToEntity(List<AnswerForm> aForms, int resultId){
		List<AnswerEntity> entitys = new ArrayList<AnswerEntity>();
		for(AnswerForm answerForm: aForms){
			AnswerEntity entity = new AnswerEntity();
			BeanUtils.copyProperties(answerForm, entity);
			entity.setResultId(resultId);
			entitys.add(entity);
		}
		return entitys;
	}

}
