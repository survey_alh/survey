package jp.co.surveyAlh.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.surveyAlh.dto.ResultDto;
import jp.co.surveyAlh.entity.ResultEntity;
import jp.co.surveyAlh.form.AnswerForm;
import jp.co.surveyAlh.mapper.ResultMapper;

@Service
public class ResultService {

	@Autowired
	private ResultMapper resultMapper;

	public List<ResultDto> getResult(int userId){
		List<ResultEntity> rEntities = resultMapper.getResults(userId);
		List<ResultDto> result = convertToDto(rEntities);
		return result;
	}

	private List<ResultDto> convertToDto(List<ResultEntity> rEntities){
		List<ResultDto> dtos = new ArrayList<ResultDto>();
		for(ResultEntity result: rEntities){
			ResultDto dto = new ResultDto();
			BeanUtils.copyProperties(result, dto);
			dtos.add(dto);
		}
		return dtos;
	}
	public int setResult(List<AnswerForm> aForms){
		ResultEntity rEntity = new ResultEntity();
		rEntity.setDriveNumber(0);
		rEntity.setVolunteerNumber(0);
		rEntity.setCreateNumber(0);
		rEntity.setAnalyzeNumber(0);
		rEntity.setSurveyId(aForms.get(0).getSurveyId());
		rEntity.setUserId(aForms.get(0).getUserId());
		for(AnswerForm answerForm: aForms){
			rEntity.setDriveNumber(rEntity.getDriveNumber() + answerForm.getDriveNumber());
			rEntity.setVolunteerNumber(rEntity.getVolunteerNumber() + answerForm.getVolunteerNumber());
			rEntity.setCreateNumber(rEntity.getCreateNumber() + answerForm.getCreateNumber());
			rEntity.setAnalyzeNumber(rEntity.getAnalyzeNumber() + answerForm.getAnalyzeNumber());
		}

		resultMapper.setResult(rEntity);
		ResultEntity maxEntity = resultMapper.getMaxId();
		return maxEntity.getId();
	}

	/**
	 * @author ikegami.masaki
	 * @param userId ユーザーのID
	 *
	 * @return 特定のユーザーの最新診断結果
	 * */
	public ResultDto getNewResult(int userId) {

		ResultEntity entity = resultMapper.getNewResult(userId);

		if (entity != null) {

			ResultDto dto = new ResultDto();
			BeanUtils.copyProperties(entity, dto);
			return dto;
		} else {

			return null;
		}
	}
}
