package jp.co.surveyAlh.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.surveyAlh.dto.SurveyDto;
import jp.co.surveyAlh.entity.QuestionEntity;
import jp.co.surveyAlh.entity.SurveyEntity;
import jp.co.surveyAlh.mapper.QuestionMapper;
import jp.co.surveyAlh.mapper.SurveyMapper;

@Service
public class SurveyService {

	@Autowired
	private SurveyMapper surveyMapper;

	@Autowired
	private QuestionMapper questionMapper;

	public List<SurveyDto> getSurvey(int surveyId){
		//mapperはtable毎に分けたため、必要な情報を二回に分けて取ってくる
		SurveyEntity sEntity = surveyMapper.getSurvey(surveyId);
		List<QuestionEntity> qEntities = questionMapper.getQuestions(surveyId);

		//とってきた情報をDtoにつめ直してcontrollerに返す
		List<SurveyDto> survey = convertToDto(sEntity, qEntities);
		return survey;
	}

	/**
	 * @author ikegami.masaki
	 *
	 * @return すべてのSurvey情報を取得
	 * （アンケートの問題はなし）
	 * */
	public List<SurveyDto> getSurveys() {

		List<SurveyEntity> entity = surveyMapper.getSurveys();
		List<SurveyDto> ret = convertToDto(entity);

		return ret;
	}

	/**
	 * @author ikegami.masaki
	 * @param sEntity DBから検索したEntityオブジェクト
	 *
	 * @return Dtoに詰め替えたものをListで返す
	 * */
	private List<SurveyDto> convertToDto(List<SurveyEntity> sEntity){
		List<SurveyDto> ret = new ArrayList<SurveyDto>();
		for(SurveyEntity entity: sEntity){
			SurveyDto dto = new SurveyDto();
			BeanUtils.copyProperties(entity, dto);
			// BeanUtilsではおぎなえないものを追加
			dto.setSurveyTitle(entity.getName());
			ret.add(dto);
		}
		return ret;
	}

	private List<SurveyDto> convertToDto(SurveyEntity sEntity, List<QuestionEntity> qEntities){
		List<SurveyDto> dtos = new ArrayList<SurveyDto>();
		for(QuestionEntity question: qEntities){
			SurveyDto dto = new SurveyDto();
			BeanUtils.copyProperties(question, dto);
			dto.setSurveyTitle(sEntity.getName());
			dto.setSurveyId(sEntity.getId());
			dtos.add(dto);
		}
		return dtos;
	}
}
