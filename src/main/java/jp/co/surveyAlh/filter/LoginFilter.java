package jp.co.surveyAlh.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.surveyAlh.dto.UserDto;

public class LoginFilter implements  Filter {

	// ログイン制限をかけないURL
	private final String[] NOT_FILTER_LIST = {"/login", "/signup", "/logout", ".*/css.*"};

	public void init(FilterConfig filterConfig) throws ServletException {}

	public void doFilter ( ServletRequest request, ServletResponse response, FilterChain chain )
				throws IOException, ServletException {

		// 使用する値を設定
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		UserDto user = (UserDto) req.getSession().getAttribute("loginUser");
		HttpSession session = req.getSession();
		List<String> messages = new ArrayList<String>();

		// リダイレクトのためにルートのURLを取得
		String contextUrl = req.getContextPath();

		//現在のURLを取得
		String nowUrl = req.getServletPath();

		//制限なしのURL
		for (String url : NOT_FILTER_LIST) {
			if (nowUrl.matches(url)) {
				chain.doFilter(request, response);
				return;
			}
		}

		// (/login以外を)ログイン中のみに制限
		if (user != null ) {

			chain.doFilter(request, response);
		} else {

			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			res.sendRedirect(contextUrl + "/login");
		}
		return ;
	}

    public void destroy() {}
}

