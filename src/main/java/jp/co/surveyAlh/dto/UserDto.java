package jp.co.surveyAlh.dto;

import java.sql.Date;

public class UserDto {
	private int id;
	private String loginId;
	private String name;
	private String password;
	private String passwordConfirmation;
	private Date createdDate;
	private Date updatedDate;
	// 診断結果の値
	private int driveNumber;
	private int volunteerNumber;
	private int analyzeNumber;
	private int createNumber;


	public int getDriveNumber() {
		return driveNumber;
	}

	public void setDriveNumber(int driveNumber) {
		this.driveNumber = driveNumber;
	}

	public int getVolunteerNumber() {
		return volunteerNumber;
	}

	public void setVolunteerNumber(int volunteerNumber) {
		this.volunteerNumber = volunteerNumber;
	}

	public int getAnalyzeNumber() {
		return analyzeNumber;
	}

	public void setAnalyzeNumber(int analyzeNumber) {
		this.analyzeNumber = analyzeNumber;
	}

	public int getCreateNumber() {
		return createNumber;
	}

	public void setCreateNumber(int createNumber) {
		this.createNumber = createNumber;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}
	public void setPasswordConfirmtion(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}


	/**
	 * 診断結果から性格のタイプを返す
	 * */
	public String getPersonalityType() {

		// 診断しているかを確認
		if (!checkResult()) {
			return "";
		}

		if (driveNumber < volunteerNumber) {
			return "ボランティア";
		} else if(driveNumber > volunteerNumber) {
			return "ドライブ";
		} else {
			return "中立的";
		}
	}

	/**
	 * 診断結果から思考のタイプを返す
	 * */
	public String getThinkingType() {

		// 診断しているかを確認
		if (!checkResult()) {
			return "診断していません";
		}

		if (analyzeNumber < createNumber) {
			return "クリエイト";
		} else if(analyzeNumber > createNumber) {
			return "アナライズ";
		} else {
			return "中立的";
		}
	}

	/**
	 * 診断しているかを判断
	 * */
	public boolean checkResult() {
		int total = driveNumber + volunteerNumber +analyzeNumber + createNumber;
		if (total == 30) {
			return true;
		} else {
			return false;
		}
	}
}

