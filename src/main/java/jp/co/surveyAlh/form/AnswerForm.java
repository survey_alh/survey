package jp.co.surveyAlh.form;

public class AnswerForm {
	private int questionId;
	private int userId;
	private int surveyId;
	private int driveNumber;
	private int volunteerNumber;
	private int analyzeNumber;
	private int createNumber;

	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}
	public int getDriveNumber() {
		return driveNumber;
	}
	public void setDriveNumber(int driveNumber) {
		this.driveNumber = driveNumber;
	}
	public int getVolunteerNumber() {
		return volunteerNumber;
	}
	public void setVolunteerNumber(int volunteerNumber) {
		this.volunteerNumber = volunteerNumber;
	}
	public int getAnalyzeNumber() {
		return analyzeNumber;
	}
	public void setAnalyzeNumber(int analyzeNumber) {
		this.analyzeNumber = analyzeNumber;
	}
	public int getCreateNumber() {
		return createNumber;
	}
	public void setCreateNumber(int createNumber) {
		this.createNumber = createNumber;
	}


}
