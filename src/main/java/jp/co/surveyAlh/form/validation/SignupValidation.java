package jp.co.surveyAlh.form.validation;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import jp.co.surveyAlh.dto.UserDto;
import jp.co.surveyAlh.form.UserForm;
import jp.co.surveyAlh.service.UserService;

public class SignupValidation {

	/**
	 * @author ikegami.masaki
	 * @param form フォームから受け取ったFormオブジェクト
	 * @param messages エラーメッセージを格納するList
	 *
	 * @return 値が正しいかを真偽値でかえす
	 * */
	public static boolean isValid(UserForm form, List<String> messages, UserService userService ){

		checkLoginId(form, messages, userService);
		checkName(form, messages);
		checkPassword(form, messages);
		checkPasswordConfirmtion(form, messages);

		if (messages.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

	private static void checkLoginId(UserForm form, List<String> messages, UserService userService) {

		String loginId = form.getLoginId();

		if (StringUtils.isBlank(loginId)) {
			messages.add("ログインIDを入力してください");
			return;
		}
		if (loginId.matches("[a-zA-z0-9]{1,20}") == false) {
			messages.add("ログインIDは半角英数字で入力してください");
			return;
		}
		UserDto user = userService.getUserByLoginId(loginId);
		if (user != null) {
			if (user.getId() != form.getId()) {
				messages.add("ログインIDはすでに使用されています");
				return;
			}
		}
	}

	private static void checkName(UserForm form, List<String> messages) {

		String name = form.getName();

		if (StringUtils.isBlank(name)) {
			messages.add("名前を入力してください");
			return;
		}
		if (name.length() > 10) {
			messages.add("名前は10文字以下で入力してください");
			return;
		}
	}

	/**
	 * passwordのチェック
	 * 空文字(変更可能)
	 * 半角英数字（記号可）
	 * 確認パスワードと同じか
	 *
	 * */
	private static void checkPassword(UserForm form, List<String> messages) {

		String password = form.getPassword();


		if (StringUtils.isBlank(password)) {
			messages.add("パスワードを入力してください");
			return;
		}
		if (password.matches("[-_@+*;:#$%&a-zA-Z0-9]{6,20}") == false  ) {
			messages.add("パスワードは半角英数字(記号可)で入力してください");
		}
	}

	/**
	 * passwordのチェック
	 * 空文字(変更可能)
	 * 半角英数字（記号可）
	 * 確認パスワードと同じか
	 *
	 * */
	private static void checkPasswordConfirmtion(UserForm form, List<String> messages) {

		String password = form.getPassword();
		String passwordConfirmtion = form.getPasswordConfirmation();

		if (StringUtils.isBlank(passwordConfirmtion)) {
			messages.add("確認用パスワードを入力してください");
			return;
		}
		if (password.matches(passwordConfirmtion) == false) {
			messages.add("パスワードと確認用パスワードが一致しません");
		}

	}

}
