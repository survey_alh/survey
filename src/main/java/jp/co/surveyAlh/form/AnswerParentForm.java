package jp.co.surveyAlh.form;

import java.util.List;

public class AnswerParentForm {

	private List<AnswerForm> answerForm;

	public List<AnswerForm> getAnswerForm() {
		return answerForm;
	}

	public void setAnswerForm(List<AnswerForm> answerForm) {
		this.answerForm = answerForm;
	}
}
