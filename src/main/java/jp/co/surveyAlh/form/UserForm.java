package jp.co.surveyAlh.form;

public class UserForm {

	private int id;
	private String loginId;
	private String name;
	private String password;
	private String passwordConfirmation;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}
	public void setPasswordConfirmtion(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}
}
