package jp.co.surveyAlh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.surveyAlh.dto.ResultDto;
import jp.co.surveyAlh.dto.SurveyDto;
import jp.co.surveyAlh.dto.UserDto;
import jp.co.surveyAlh.service.ResultService;
import jp.co.surveyAlh.service.SurveyService;

@Controller
public class MypageController {

	@Autowired
	private SurveyService surveyService;
	@Autowired
	private ResultService resultService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String mypage(Model model, HttpServletRequest req) {

		// ログインユーザーのIDを取得
		HttpSession session = req.getSession();
		UserDto loginUser = (UserDto) session.getAttribute("loginUser");
		int userId = loginUser.getId();

		// 最新の診断結果を取得
		ResultDto newResult = resultService.getNewResult(userId);
		List<SurveyDto> surveys = surveyService.getSurveys();
		model.addAttribute("newResult", newResult);
		model.addAttribute("surveys", surveys);
		return "Mypage";
	}
}
