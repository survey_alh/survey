package jp.co.surveyAlh.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.surveyAlh.dto.UserDto;
import jp.co.surveyAlh.form.UserForm;
import jp.co.surveyAlh.service.UserService;

@Controller
public class LoginController {

	@Autowired
	private UserService userService;

	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login(Model model, HttpServletRequest req) {

		UserForm form = new UserForm();
		model.addAttribute("loginForm", form);
		return "Login";
	}

	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String login(@ModelAttribute UserForm form, Model model, HttpServletRequest req) {


		// フォーム情報を取得
		String loginId = form.getLoginId();
		String password = form.getPassword();

		// 特定のユーザーを取得
		UserDto userDto = userService.getUser(loginId, password);

		if (userDto != null ) {

			// ログイン情報をセッションに設定
			req.getSession().setAttribute("loginUser", userDto);
			return "redirect:/";
		} else {

			model.addAttribute("loginForm", form);
			return "Login";
		}
	}

	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public String logout( HttpServletRequest req) {
		HttpSession session = req.getSession();
		session.setAttribute("loginUser", null);
		return "redirect:/login";
	}
}