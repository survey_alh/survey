package jp.co.surveyAlh.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jp.co.surveyAlh.dto.SurveyDto;
import jp.co.surveyAlh.dto.UserDto;
import jp.co.surveyAlh.form.AnswerForm;
import jp.co.surveyAlh.form.AnswerParentForm;
import jp.co.surveyAlh.service.AnswerService;
import jp.co.surveyAlh.service.SurveyService;

@Controller
public class SurveyController {

	@Autowired
	private SurveyService surveyService;

	@Autowired
	private AnswerService answerService;

	@RequestMapping(value="survey", method = RequestMethod.GET)
	public String doSurvey(@RequestParam("id")int surveyId, Model model,HttpServletRequest req){
		//survey.jspで表示する質問を取ってくる(Dtoの中身は確認しておいてください)
		List<SurveyDto> surveys = surveyService.getSurvey(surveyId);

		//回答フォームの初期設定をしてとって来る
		List<AnswerForm> aForms = answerService.getAnswerForm(surveys);
		AnswerParentForm apForm = new AnswerParentForm();
		apForm.setAnswerForm(aForms);
		model.addAttribute("surveys", surveys);
		model.addAttribute("apForm",apForm);
		return "Survey";
	}

	@RequestMapping(value="survey", method = RequestMethod.POST)
	public String postResult(@ModelAttribute AnswerParentForm apForm, Model model){
		List<AnswerForm> aForms = apForm.getAnswerForm();

		answerService.setAnswer(aForms);

		return "redirect:/";
	}
}
