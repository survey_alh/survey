package jp.co.surveyAlh.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.surveyAlh.dto.UserDto;
import jp.co.surveyAlh.service.UserService;

@Controller
public class UsersController {

	@Autowired
	private UserService userService;

	@RequestMapping(value="/users", method=RequestMethod.GET)
	public String users(Model model) {

		List<UserDto> users = userService.getUsers();

		model.addAttribute("users", users);
		return "Users";
	}
}
