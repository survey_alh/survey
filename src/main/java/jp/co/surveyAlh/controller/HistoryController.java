package jp.co.surveyAlh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.surveyAlh.dto.ResultDto;
import jp.co.surveyAlh.dto.UserDto;
import jp.co.surveyAlh.service.ResultService;

@Controller
public class HistoryController {
	@Autowired
	private ResultService resultService;

	@RequestMapping(value="history", method = RequestMethod.GET)
	public String doHistory(Model model, HttpServletRequest req){
		UserDto userDto = (UserDto) req.getSession().getAttribute("loginUser");
		List<ResultDto> results = resultService.getResult(userDto.getId());

		model.addAttribute("results", results);
		return "History";
	}
}
