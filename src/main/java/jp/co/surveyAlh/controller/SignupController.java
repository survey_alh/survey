package jp.co.surveyAlh.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.surveyAlh.dto.UserDto;
import jp.co.surveyAlh.form.UserForm;
import jp.co.surveyAlh.form.validation.SignupValidation;
import jp.co.surveyAlh.service.UserService;

@Controller
public class SignupController {

	@Autowired
	private UserService userService;

	// 新規登録画面表示
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signUp(Model model) {

		// フォームの作成
		UserForm form = new UserForm();
		model.addAttribute("userForm", form);
		return "Signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signup(@ModelAttribute UserForm form, Model model, HttpServletRequest req ) {

		List<String> messages = new ArrayList<String>();

		if (SignupValidation.isValid(form, messages, userService)) {

			UserDto userDto = new UserDto();
			BeanUtils.copyProperties(form, userDto);
			userService.insert(userDto);
			UserDto loginUser = userService.getUser(form.getLoginId(), form.getPassword());

			// ログイン処理
			req.getSession().setAttribute("loginUser", loginUser);
			return "redirect:/";
		} else {

			model.addAttribute("errorMessages", messages);
			return "Signup";
		}
	}
}